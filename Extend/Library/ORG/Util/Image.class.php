<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id: Image.class.php 2708 2012-02-06 04:10:11Z liu21st $

/**
  +------------------------------------------------------------------------------
 * 脥录脧帽虏脵脳梅脌脿驴芒
  +------------------------------------------------------------------------------
 * @category   ORG
 * @package  ORG
 * @subpackage  Util
 * @author    liu21st <liu21st@gmail.com>
 * @version   $Id: Image.class.php 2708 2012-02-06 04:10:11Z liu21st $
  +------------------------------------------------------------------------------
 */
class Image {

    /**
      +----------------------------------------------------------
     * 脠隆碌脙脥录脧帽脨脜脧垄
     *
      +----------------------------------------------------------
     * @static
     * @access public
      +----------------------------------------------------------
     * @param string $image 脥录脧帽脦脛录镁脙没
      +----------------------------------------------------------
     * @return mixed
      +----------------------------------------------------------
     */

    static function getImageInfo($img) {
        $imageInfo = getimagesize($img);
        if ($imageInfo !== false) {
            $imageType = strtolower(substr(image_type_to_extension($imageInfo[2]), 1));
            $imageSize = filesize($img);
            $info = array(
                "width" => $imageInfo[0],
                "height" => $imageInfo[1],
                "type" => $imageType,
                "size" => $imageSize,
                "mime" => $imageInfo['mime']
            );
            return $info;
        } else {
            return false;
        }
    }

    /**
      +----------------------------------------------------------
     * 脦陋脥录脝卢脤铆录脫脣庐脫隆
      +----------------------------------------------------------
     * @static public
      +----------------------------------------------------------
     * @param string $source 脭颅脦脛录镁脙没
     * @param string $water  脣庐脫隆脥录脝卢
     * @param string $$savename  脤铆录脫脣庐脫隆潞贸碌脛脥录脝卢脙没
     * @param string $alpha  脣庐脫隆碌脛脥赂脙梅露脠
      +----------------------------------------------------------
     * @return void
      +----------------------------------------------------------
     */
    static public function water($source, $water, $savename=null, $alpha=80) {
        //录矛虏茅脦脛录镁脢脟路帽麓忙脭脷
        if (!file_exists($source) || !file_exists($water))
            return false;

        //脥录脝卢脨脜脧垄
        $sInfo = self::getImageInfo($source);
        $wInfo = self::getImageInfo($water);

        //脠莽鹿没脥录脝卢脨隆脫脷脣庐脫隆脥录脝卢拢卢虏禄脡煤鲁脡脥录脝卢
        if ($sInfo["width"] < $wInfo["width"] || $sInfo['height'] < $wInfo['height'])
            return false;

        //陆篓脕垄脥录脧帽
        $sCreateFun = "imagecreatefrom" . $sInfo['type'];
        $sImage = $sCreateFun($source);
        $wCreateFun = "imagecreatefrom" . $wInfo['type'];
        $wImage = $wCreateFun($water);

        //脡猫露篓脥录脧帽碌脛禄矛脡芦脛拢脢陆
        imagealphablending($wImage, true);

        //脥录脧帽脦禄脰脙,脛卢脠脧脦陋脫脪脧脗陆脟脫脪露脭脝毛
        $posY = $sInfo["height"] - $wInfo["height"];
        $posX = $sInfo["width"] - $wInfo["width"];

        //脡煤鲁脡禄矛潞脧脥录脧帽
        imagecopymerge($sImage, $wImage, $posX, $posY, 0, 0, $wInfo['width'], $wInfo['height'], $alpha);

        //脢盲鲁枚脥录脧帽
        $ImageFun = 'Image' . $sInfo['type'];
        //脠莽鹿没脙禄脫脨赂酶鲁枚卤拢麓忙脦脛录镁脙没拢卢脛卢脠脧脦陋脭颅脥录脧帽脙没
        if (!$savename) {
            $savename = $source;
            @unlink($source);
        }
        //卤拢麓忙脥录脧帽
        $ImageFun($sImage, $savename);
        imagedestroy($sImage);
    }

    function showImg($imgFile, $text='', $x='10', $y='10', $alpha='50') {
        //禄帽脠隆脥录脧帽脦脛录镁脨脜脧垄
        //2007/6/26 脭枚录脫脥录脝卢脣庐脫隆脢盲鲁枚拢卢$text脦陋脥录脝卢碌脛脥锚脮没脗路戮露录麓驴脡
        $info = Image::getImageInfo($imgFile);
        if ($info !== false) {
            $createFun = str_replace('/', 'createfrom', $info['mime']);
            $im = $createFun($imgFile);
            if ($im) {
                $ImageFun = str_replace('/', '', $info['mime']);
                //脣庐脫隆驴陋脢录
                if (!empty($text)) {
                    $tc = imagecolorallocate($im, 0, 0, 0);
                    if (is_file($text) && file_exists($text)) {//脜脨露脧$text脢脟路帽脢脟脥录脝卢脗路戮露
                        // 脠隆碌脙脣庐脫隆脨脜脧垄
                        $textInfo = Image::getImageInfo($text);
                        $createFun2 = str_replace('/', 'createfrom', $textInfo['mime']);
                        $waterMark = $createFun2($text);
                        //$waterMark=imagecolorallocatealpha($text,255,255,0,50);
                        $imgW = $info["width"];
                        $imgH = $info["width"] * $textInfo["height"] / $textInfo["width"];
                        //$y	=	($info["height"]-$textInfo["height"])/2;
                        //脡猫脰脙脣庐脫隆碌脛脧脭脢戮脦禄脰脙潞脥脥赂脙梅露脠脰搂鲁脰赂梅脰脰脥录脝卢赂帽脢陆
                        imagecopymerge($im, $waterMark, $x, $y, 0, 0, $textInfo['width'], $textInfo['height'], $alpha);
                    } else {
                        imagestring($im, 80, $x, $y, $text, $tc);
                    }
                    //ImageDestroy($tc);
                }
                //脣庐脫隆陆谩脢酶
                if ($info['type'] == 'png' || $info['type'] == 'gif') {
                    imagealphablending($im, FALSE); //脠隆脧没脛卢脠脧碌脛禄矛脡芦脛拢脢陆
                    imagesavealpha($im, TRUE); //脡猫露篓卤拢麓忙脥锚脮没碌脛 alpha 脥篓碌脌脨脜脧垄
                }
                Header("Content-type: " . $info['mime']);
                $ImageFun($im);
                @ImageDestroy($im);
                return;
            }

            //卤拢麓忙脥录脧帽
            $ImageFun($sImage, $savename);
            imagedestroy($sImage);
            //禄帽脠隆禄貌脮脽麓麓陆篓脥录脧帽脦脛录镁脢搂掳脺脭貌脡煤鲁脡驴脮掳脳PNG脥录脝卢
            $im = imagecreatetruecolor(80, 30);
            $bgc = imagecolorallocate($im, 255, 255, 255);
            $tc = imagecolorallocate($im, 0, 0, 0);
            imagefilledrectangle($im, 0, 0, 150, 30, $bgc);
            imagestring($im, 4, 5, 5, "no pic", $tc);
            Image::output($im);
            return;
        }
    }

    /**
      +----------------------------------------------------------
     * 脡煤鲁脡脣玫脗脭脥录
      +----------------------------------------------------------
     * @static
     * @access public
      +----------------------------------------------------------
     * @param string $image  脭颅脥录
     * @param string $type 脥录脧帽赂帽脢陆
     * @param string $thumbname 脣玫脗脭脥录脦脛录镁脙没
     * @param string $maxWidth  驴铆露脠
     * @param string $maxHeight  赂脽露脠
     * @param string $position 脣玫脗脭脥录卤拢麓忙脛驴脗录
     * @param boolean $interlace 脝么脫脙赂么脨脨脡篓脙猫
      +----------------------------------------------------------
     * @return void
      +----------------------------------------------------------
     */
    static function thumb($image, $thumbname, $type='', $maxWidth=200, $maxHeight=50, $interlace=true) {
        // 禄帽脠隆脭颅脥录脨脜脧垄
        $info = Image::getImageInfo($image);
        if ($info !== false) {
            $srcWidth = $info['width'];
            $srcHeight = $info['height'];
            $type = empty($type) ? $info['type'] : $type;
            $type = strtolower($type);
            $interlace = $interlace ? 1 : 0;
            unset($info);
            $scale = min($maxWidth / $srcWidth, $maxHeight / $srcHeight); // 录脝脣茫脣玫路脜卤脠脌媒
            if ($scale >= 1) {
                // 鲁卢鹿媒脭颅脥录麓贸脨隆虏禄脭脵脣玫脗脭
                $width = $srcWidth;
                $height = $srcHeight;
            } else {
                // 脣玫脗脭脥录鲁脽麓莽
                $width = (int) ($srcWidth * $scale);
                $height = (int) ($srcHeight * $scale);
            }

            // 脭脴脠毛脭颅脥录
            $createFun = 'ImageCreateFrom' . ($type == 'jpg' ? 'jpeg' : $type);
            $srcImg = $createFun($image);

            //麓麓陆篓脣玫脗脭脥录
            if ($type != 'gif' && function_exists('imagecreatetruecolor'))
                $thumbImg = imagecreatetruecolor($width, $height);
            else
                $thumbImg = imagecreate($width, $height);

            // 赂麓脰脝脥录脝卢
            if (function_exists("ImageCopyResampled"))
                imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
            else
                imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
            if ('gif' == $type || 'png' == $type) {
                //imagealphablending($thumbImg, false);//脠隆脧没脛卢脠脧碌脛禄矛脡芦脛拢脢陆
                //imagesavealpha($thumbImg,true);//脡猫露篓卤拢麓忙脥锚脮没碌脛 alpha 脥篓碌脌脨脜脧垄
                $background_color = imagecolorallocate($thumbImg, 0, 255, 0);  //  脰赂脜脡脪禄赂枚脗脤脡芦
                imagecolortransparent($thumbImg, $background_color);  //  脡猫脰脙脦陋脥赂脙梅脡芦拢卢脠么脳垄脢脥碌么赂脙脨脨脭貌脢盲鲁枚脗脤脡芦碌脛脥录
            }

            // 露脭jpeg脥录脨脦脡猫脰脙赂么脨脨脡篓脙猫
            if ('jpg' == $type || 'jpeg' == $type)
                imageinterlace($thumbImg, $interlace);

            // 脡煤鲁脡脥录脝卢
            $imageFun = 'image' . ($type == 'jpg' ? 'jpeg' : $type);
            $imageFun($thumbImg, $thumbname);
            imagedestroy($thumbImg);
            imagedestroy($srcImg);
            return $thumbname;
        }
        return false;
    }


    // 脰脨脦脛脩茅脰陇脗毛
    static function ZHbuildString($string, $filename='', $width=300, $height=28, $type='png', $fontface='simhei.ttf') {
        load('extend');
        //$string=mb_convert_encoding($string, "UTF-8", "GBK");
        //echo $string;
        $length = mb_strlen($string,'utf8');
        //echo $length;
        $width = ($length * 20) > $width ? $length * 20 : $width;
        $start=floor(($width-$length*20)/2);
        $im = imagecreatetruecolor($width, $height);
        $bkcolor = imagecolorallocate($im, 250, 250, 250);
        imagefill($im, 0, 0, $bkcolor);
        if (!is_file($fontface)) {
            $fontface = dirname(__FILE__) . "/" . $fontface;
        }
        for ($i = 0; $i < $length; $i++) {
            $fontcolor = imagecolorallocate($im, mt_rand(0, 120), mt_rand(0, 120), mt_rand(0, 120)); //脮芒脩霉卤拢脰陇脣忙禄煤鲁枚脌麓碌脛脩脮脡芦陆脧脡卯隆拢
            $codex = msubstr($string, $i, 1,'gbk',false);
            //echo $codex;
            imagettftext($im, 14, 0, $start+20 * $i, 20, $fontcolor, $fontface, $codex);
        }
        Image::output($im, $type, $filename);
    }


    /**
      +----------------------------------------------------------
     * 赂霉戮脻赂酶露篓碌脛脳脰路没麓庐脡煤鲁脡脥录脧帽
      +----------------------------------------------------------
     * @static
     * @access public
      +----------------------------------------------------------
     * @param string $string  脳脰路没麓庐
     * @param string $size  脥录脧帽麓贸脨隆 width,height 禄貌脮脽 array(width,height)
     * @param string $font  脳脰脤氓脨脜脧垄 fontface,fontsize 禄貌脮脽 array(fontface,fontsize)
     * @param string $type 脥录脧帽赂帽脢陆 脛卢脠脧PNG
     * @param integer $disturb 脢脟路帽赂脡脠脜 1 碌茫赂脡脠脜 2 脧脽赂脡脠脜 3 赂麓潞脧赂脡脠脜 0 脦脼赂脡脠脜
     * @param bool $border  脢脟路帽录脫卤脽驴貌 array(color)
      +----------------------------------------------------------
     * @return string
      +----------------------------------------------------------
     */
    static function buildString($string, $rgb=array(), $filename='', $type='png', $disturb=1, $border=true) {
        if (is_string($size))
            $size = explode(',', $size);
        $width = $size[0];
        $height = $size[1];
        if (is_string($font))
            $font = explode(',', $font);
        $fontface = $font[0];
        $fontsize = $font[1];
        $length = strlen($string);
        $width = ($length * 9 + 10) > $width ? $length * 9 + 10 : $width;
        $height = 22;
        if ($type != 'gif' && function_exists('imagecreatetruecolor')) {
            $im = @imagecreatetruecolor($width, $height);
        } else {
            $im = @imagecreate($width, $height);
        }
        if (empty($rgb)) {
            $color = imagecolorallocate($im, 102, 104, 104);
        } else {
            $color = imagecolorallocate($im, $rgb[0], $rgb[1], $rgb[2]);
        }
        $backColor = imagecolorallocate($im, 255, 255, 255);    //卤鲁戮掳脡芦拢篓脣忙禄煤拢漏
        $borderColor = imagecolorallocate($im, 100, 100, 100);                    //卤脽驴貌脡芦
        $pointColor = imagecolorallocate($im, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));                 //碌茫脩脮脡芦

        @imagefilledrectangle($im, 0, 0, $width - 1, $height - 1, $backColor);
        @imagerectangle($im, 0, 0, $width - 1, $height - 1, $borderColor);
        @imagestring($im, 5, 5, 3, $string, $color);
        if (!empty($disturb)) {
            // 脤铆录脫赂脡脠脜
            if ($disturb = 1 || $disturb = 3) {
                for ($i = 0; $i < 25; $i++) {
                    imagesetpixel($im, mt_rand(0, $width), mt_rand(0, $height), $pointColor);
                }
            } elseif ($disturb = 2 || $disturb = 3) {
                for ($i = 0; $i < 10; $i++) {
                    imagearc($im, mt_rand(-10, $width), mt_rand(-10, $height), mt_rand(30, 300), mt_rand(20, 200), 55, 44, $pointColor);
                }
            }
        }
        Image::output($im, $type, $filename);
    }

    /**
      +----------------------------------------------------------
     * 脡煤鲁脡脥录脧帽脩茅脰陇脗毛
      +----------------------------------------------------------
     * @static
     * @access public
      +----------------------------------------------------------
     * @param string $length  脦禄脢媒
     * @param string $mode  脌脿脨脥
     * @param string $type 脥录脧帽赂帽脢陆
     * @param string $width  驴铆露脠
     * @param string $height  赂脽露脠
      +----------------------------------------------------------
     * @return string
      +----------------------------------------------------------
     */
    static function buildImageVerify($length=4, $mode=1, $type='png', $width=48, $height=22, $verifyName='verify') {
        import('ORG.Util.String');
        $randval = String::randString($length, $mode);
        $_SESSION[$verifyName] = md5($randval);
        $width = ($length * 10 + 10) > $width ? $length * 10 + 10 : $width;
        if ($type != 'gif' && function_exists('imagecreatetruecolor')) {
            $im = imagecreatetruecolor($width, $height);
        } else {
            $im = imagecreate($width, $height);
        }
        $r = Array(225, 255, 255, 223);
        $g = Array(225, 236, 237, 255);
        $b = Array(225, 236, 166, 125);
        $key = mt_rand(0, 3);

        $backColor = imagecolorallocate($im, $r[$key], $g[$key], $b[$key]);    //卤鲁戮掳脡芦拢篓脣忙禄煤拢漏
        $borderColor = imagecolorallocate($im, 100, 100, 100);                    //卤脽驴貌脡芦
        imagefilledrectangle($im, 0, 0, $width - 1, $height - 1, $backColor);
        imagerectangle($im, 0, 0, $width - 1, $height - 1, $borderColor);
        $stringColor = imagecolorallocate($im, mt_rand(0, 200), mt_rand(0, 120), mt_rand(0, 120));
        // 赂脡脠脜
        for ($i = 0; $i < 10; $i++) {
            imagearc($im, mt_rand(-10, $width), mt_rand(-10, $height), mt_rand(30, 300), mt_rand(20, 200), 55, 44, $stringColor);
        }
        for ($i = 0; $i < 25; $i++) {
            imagesetpixel($im, mt_rand(0, $width), mt_rand(0, $height), $stringColor);
        }
        for ($i = 0; $i < $length; $i++) {
            imagestring($im, 5, $i * 10 + 5, mt_rand(1, 8), $randval{$i}, $stringColor);
        }
        Image::output($im, $type);
    }

    // 脰脨脦脛脩茅脰陇脗毛
    static function GBVerify($length=4, $type='png', $width=180, $height=50, $fontface='simhei.ttf', $verifyName='verify') {
        import('ORG.Util.String');
        $code = String::randString($length, 4);
        $width = ($length * 45) > $width ? $length * 45 : $width;
        $_SESSION[$verifyName] = md5($code);
        $im = imagecreatetruecolor($width, $height);
        $borderColor = imagecolorallocate($im, 100, 100, 100);                    //卤脽驴貌脡芦
        $bkcolor = imagecolorallocate($im, 250, 250, 250);
        imagefill($im, 0, 0, $bkcolor);
        @imagerectangle($im, 0, 0, $width - 1, $height - 1, $borderColor);
        // 赂脡脠脜
        for ($i = 0; $i < 15; $i++) {
            $fontcolor = imagecolorallocate($im, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            imagearc($im, mt_rand(-10, $width), mt_rand(-10, $height), mt_rand(30, 300), mt_rand(20, 200), 55, 44, $fontcolor);
        }
        for ($i = 0; $i < 255; $i++) {
            $fontcolor = imagecolorallocate($im, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            imagesetpixel($im, mt_rand(0, $width), mt_rand(0, $height), $fontcolor);
        }
        if (!is_file($fontface)) {
            $fontface = dirname(__FILE__) . "/" . $fontface;
        }
        for ($i = 0; $i < $length; $i++) {
            $fontcolor = imagecolorallocate($im, mt_rand(0, 120), mt_rand(0, 120), mt_rand(0, 120)); //脮芒脩霉卤拢脰陇脣忙禄煤鲁枚脌麓碌脛脩脮脡芦陆脧脡卯隆拢
            $codex = String::msubstr($code, $i, 1);
            imagettftext($im, mt_rand(16, 20), mt_rand(-60, 60), 40 * $i + 20, mt_rand(30, 35), $fontcolor, $fontface, $codex);
        }
        Image::output($im, $type);
    }

    /**
      +----------------------------------------------------------
     * 掳脩脥录脧帽脳陋禄禄鲁脡脳脰路没脧脭脢戮
      +----------------------------------------------------------
     * @static
     * @access public
      +----------------------------------------------------------
     * @param string $image  脪陋脧脭脢戮碌脛脥录脧帽
     * @param string $type  脥录脧帽脌脿脨脥拢卢脛卢脠脧脳脭露炉禄帽脠隆
      +----------------------------------------------------------
     * @return string
      +----------------------------------------------------------
     */
    static function showASCIIImg($image, $string='', $type='') {
        $info = Image::getImageInfo($image);
        if ($info !== false) {
            $type = empty($type) ? $info['type'] : $type;
            unset($info);
            // 脭脴脠毛脭颅脥录
            $createFun = 'ImageCreateFrom' . ($type == 'jpg' ? 'jpeg' : $type);
            $im = $createFun($image);
            $dx = imagesx($im);
            $dy = imagesy($im);
            $i = 0;
            $out = '<span style="padding:0px;margin:0;line-height:100%;font-size:1px;">';
            set_time_limit(0);
            for ($y = 0; $y < $dy; $y++) {
                for ($x = 0; $x < $dx; $x++) {
                    $col = imagecolorat($im, $x, $y);
                    $rgb = imagecolorsforindex($im, $col);
                    $str = empty($string) ? '*' : $string[$i++];
                    $out .= sprintf('<span style="margin:0px;color:#%02x%02x%02x">' . $str . '</span>', $rgb['red'], $rgb['green'], $rgb['blue']);
                }
                $out .= "<br>\n";
            }
            $out .= '</span>';
            imagedestroy($im);
            return $out;
        }
        return false;
    }

    /**
      +----------------------------------------------------------
     * 脡煤鲁脡UPC-A脤玫脨脦脗毛
      +----------------------------------------------------------
     * @static
      +----------------------------------------------------------
     * @param string $type 脥录脧帽赂帽脢陆
     * @param string $type 脥录脧帽赂帽脢陆
     * @param string $lw  碌楼脭陋驴铆露脠
     * @param string $hi   脤玫脗毛赂脽露脠
      +----------------------------------------------------------
     * @return string
      +----------------------------------------------------------
     */
    static function UPCA($code, $type='png', $lw=2, $hi=100) {
        static $Lencode = array('0001101', '0011001', '0010011', '0111101', '0100011',
    '0110001', '0101111', '0111011', '0110111', '0001011');
        static $Rencode = array('1110010', '1100110', '1101100', '1000010', '1011100',
    '1001110', '1010000', '1000100', '1001000', '1110100');
        $ends = '101';
        $center = '01010';
        /* UPC-A Must be 11 digits, we compute the checksum. */
        if (strlen($code) != 11) {
            die("UPC-A Must be 11 digits.");
        }
        /* Compute the EAN-13 Checksum digit */
        $ncode = '0' . $code;
        $even = 0;
        $odd = 0;
        for ($x = 0; $x < 12; $x++) {
            if ($x % 2) {
                $odd += $ncode[$x];
            } else {
                $even += $ncode[$x];
            }
        }
        $code.= ( 10 - (($odd * 3 + $even) % 10)) % 10;
        /* Create the bar encoding using a binary string */
        $bars = $ends;
        $bars.=$Lencode[$code[0]];
        for ($x = 1; $x < 6; $x++) {
            $bars.=$Lencode[$code[$x]];
        }
        $bars.=$center;
        for ($x = 6; $x < 12; $x++) {
            $bars.=$Rencode[$code[$x]];
        }
        $bars.=$ends;
        /* Generate the Barcode Image */
        if ($type != 'gif' && function_exists('imagecreatetruecolor')) {
            $im = imagecreatetruecolor($lw * 95 + 30, $hi + 30);
        } else {
            $im = imagecreate($lw * 95 + 30, $hi + 30);
        }
        $fg = ImageColorAllocate($im, 0, 0, 0);
        $bg = ImageColorAllocate($im, 255, 255, 255);
        ImageFilledRectangle($im, 0, 0, $lw * 95 + 30, $hi + 30, $bg);
        $shift = 10;
        for ($x = 0; $x < strlen($bars); $x++) {
            if (($x < 10) || ($x >= 45 && $x < 50) || ($x >= 85)) {
                $sh = 10;
            } else {
                $sh = 0;
            }
            if ($bars[$x] == '1') {
                $color = $fg;
            } else {
                $color = $bg;
            }
            ImageFilledRectangle($im, ($x * $lw) + 15, 5, ($x + 1) * $lw + 14, $hi + 5 + $sh, $color);
        }
        /* Add the Human Readable Label */
        ImageString($im, 4, 5, $hi - 5, $code[0], $fg);
        for ($x = 0; $x < 5; $x++) {
            ImageString($im, 5, $lw * (13 + $x * 6) + 15, $hi + 5, $code[$x + 1], $fg);
            ImageString($im, 5, $lw * (53 + $x * 6) + 15, $hi + 5, $code[$x + 6], $fg);
        }
        ImageString($im, 4, $lw * 95 + 17, $hi - 5, $code[11], $fg);
        /* Output the Header and Content. */
        Image::output($im, $type);
    }

    static function output($im, $type='png', $filename='') {
        header("Content-type: image/" . $type);
        $ImageFun = 'image' . $type;
        if (empty($filename)) {
            $ImageFun($im);
        } else {
            $ImageFun($im, $filename);
        }
        imagedestroy($im);
    }

}