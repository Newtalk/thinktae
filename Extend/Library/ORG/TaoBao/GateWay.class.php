<?php
class GateWay{

	private $gateUrl="http://gw.api.taobao.com/router/rest";
	private $getFields;
	private $postFields;

	function __construct($getFields,$postField){
		if ($getFields || $postFields)
			$this->execute($getFields,$postFields);
	}

	public function execute($getFields,$postFields){
		if ($getFields || $postFields){
			$url=$this->gateUrl;
			if ($getFields && is_array($getFields)){
				$url.=strpos($url,'?')===false?'?':'&';
				foreach ($getFields as $key => $value) {
					$url.=$key.'='.urlencode_utf8($value).'&';
				}
				$url = substr($url, 0, -1);
			}
			return $this->curl($url,$postFields);
		}
		return array('status'=>-1,
					'data'=>'',
					'info'=>'');
	}

	private function curl($url, $postFields = null)
	{
		$result=array('status'=>1,
					'data'=>'',
					'info'=>'');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		if(strlen($url) > 5 && strtolower(substr($url,0,5)) == "https" ) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}

		if (is_array($postFields) && 0 < count($postFields))
		{
			$postBodyString = "";
			$postMultipart = false;
			foreach ($postFields as $k => $v)
			{
				if("@" != substr($v, 0, 1))				{
					$postBodyString .= "$k=" . urlencode_utf8($v) . "&";
				}
				else
				{
					$postMultipart = true;
				}
			}
			unset($k, $v);
			curl_setopt($ch, CURLOPT_POST, true);
			if ($postMultipart)
			{
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
			}
			else
			{
				curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
			}
		}
		$result['data'] = curl_exec($ch);
		
		if (curl_errno($ch))
		{
			$result['status']=0;
			$result['info']=curl_error($ch);
			return $result;
		}
		else
		{
			$httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if (200 !== $httpStatusCode)
			{
				$result['status']=0;
				$result['info']=$httpStatusCode;
				return $result;
			}
		}
		curl_close($ch);
		return $result;
	}

}


?>